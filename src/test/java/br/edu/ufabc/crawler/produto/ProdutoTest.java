package br.edu.ufabc.crawler.produto;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ProdutoTest {

	@Test
	public void deveRecomendarUmProduto() {
		Produto umProduto = new Produto(new Codigo("abc123"));
		umProduto.setNome("IPhone 6");
		Produto outroProduto = new Produto(new Codigo("def456"));
		outroProduto.setNome("Galaxy S6");

		umProduto.recomenda(outroProduto);

		assertEquals(1, umProduto.getRecomendacoes().size());
	}

}

package br.edu.ufabc.crawler.produto;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

import br.edu.ufabc.crawler.url.URL;

public class FotoTest {

	@Test
	public void fotosComMesmoEnderecoDevemSerIguais() {
		Foto umaFoto = new Foto(
				new URL(
						"http://img.submarino.com.br/produtos/01/00/item/122099/2/122099256_1GG.jpg"));
		Foto mesmaFoto = new Foto(
				new URL(
						"http://img.submarino.com.br/produtos/01/00/item/122099/2/122099256_1GG.jpg"));
		assertEquals(umaFoto, mesmaFoto);
	}

	@Test
	public void fotosComEnderecosDiferentesDevemSerDiferentes() {
		Foto umaFoto = new Foto(
				new URL(
						"http://img.submarino.com.br/produtos/01/00/item/122099/2/122099256_1GG.jpg"));
		Foto outraFoto = new Foto(
				new URL(
						"http://images.livrariasaraiva.com.br/imagemnet/imagem.aspx/?pro_id=2645609&qld=90&l=370&a=-1"));
		assertNotEquals(umaFoto, outraFoto);
	}

}

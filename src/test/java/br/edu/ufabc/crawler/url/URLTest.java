package br.edu.ufabc.crawler.url;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

public class URLTest {

	@Test
	public void duasUrlsComMesmoValorDevemSerIguais() {
		URL umaFoto = new URL(
				"http://img.submarino.com.br/produtos/01/00/item/122099/2/122099256_1GG.jpg");
		URL mesmaFoto = new URL(
				"http://img.submarino.com.br/produtos/01/00/item/122099/2/122099256_1GG.jpg");
		assertEquals(umaFoto, mesmaFoto);
	}

	@Test
	public void duasUrlsComValoresDiferentesDevemSerDiferentes() {
		URL umaFoto = new URL(
				"http://img.submarino.com.br/produtos/01/00/item/122099/2/122099256_1GG.jpg");
		URL outraFoto = new URL(
				"http://images.livrariasaraiva.com.br/imagemnet/imagem.aspx/?pro_id=2645609&qld=90&l=370&a=-1");
		assertNotEquals(umaFoto, outraFoto);
	}

}

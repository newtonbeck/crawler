package br.edu.ufabc.crawler.robot;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.url.WebURL;

public class RobotTest {

	private Robot sujeito;

	private Page page;

	private WebURL url;

	@Before
	public void antes() {
		sujeito = new Robot();
		page = mock(Page.class);
		url = mock(WebURL.class);
	}

	@Test
	public void deveVisitarUmaPaginaDeProduto() {
		when(url.getPath())
				.thenReturn("mascara-de-dormir-corujinhas/dp/4A9B19");
		boolean shouldVisit = sujeito.shouldVisit(page, url);
		assertTrue(shouldVisit);
	}

	@Test
	public void naoDeveVisitarUmaPaginaQueNaoSejaDeProdutoNemDeComprador() {
		when(url.getPath()).thenReturn(
				"categoria/acessorios/mascara-para-dormir");
		boolean shouldVisit = sujeito.shouldVisit(page, url);
		assertFalse(shouldVisit);
	}

}

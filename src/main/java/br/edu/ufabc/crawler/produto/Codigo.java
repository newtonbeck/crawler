package br.edu.ufabc.crawler.produto;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.edu.ufabc.crawler.url.URL;

public class Codigo {

	private final String valor;

	public Codigo(String valor) {
		this.valor = valor;
	}

	public String getValor() {
		return valor;
	}

	public static Codigo da(URL url) {
		Pattern pattern = Pattern.compile("/dp/[A-Z0-9]{1,}");
		Matcher matcher = pattern.matcher(url.getValor());

		String valor = null;
		if (matcher.find()) {
			valor = matcher.group().replace("/dp/", "");
		} else {
			throw new IllegalStateException(
					"Não foi encontrado um código válido para o produto da página "
							+ url.getValor());
		}
		return new Codigo(valor);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((valor == null) ? 0 : valor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Codigo other = (Codigo) obj;
		if (valor == null) {
			if (other.valor != null)
				return false;
		} else if (!valor.equals(other.valor))
			return false;
		return true;
	}

}

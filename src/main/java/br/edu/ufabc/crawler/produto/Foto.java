package br.edu.ufabc.crawler.produto;

import com.google.common.base.Objects;

import br.edu.ufabc.crawler.url.URL;

public class Foto {

	private final URL endereco;

	public Foto(URL endereco) {
		this.endereco = endereco;
	}

	public URL getEndereco() {
		return endereco;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((endereco == null) ? 0 : endereco.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Foto other = (Foto) obj;
		if (endereco == null) {
			if (other.endereco != null)
				return false;
		} else if (!endereco.equals(other.endereco))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("url", endereco)
				.toString();
	}

}

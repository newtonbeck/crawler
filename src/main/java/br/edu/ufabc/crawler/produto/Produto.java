package br.edu.ufabc.crawler.produto;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.google.common.base.Objects;

public class Produto {

	private Codigo codigo;

	private String nome;

	private Foto foto;

	private Set<Recomendacao> recomendacoes;

	public Produto(Codigo codigo) {
		this.codigo = codigo;
		recomendacoes = new HashSet<Recomendacao>();
	}

	public Codigo getCodigo() {
		return codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Foto getFoto() {
		return foto;
	}

	public void setFoto(Foto foto) {
		this.foto = foto;
	}

	public Set<Recomendacao> getRecomendacoes() {
		return Collections.unmodifiableSet(recomendacoes);
	}

	public void recomenda(Produto produto) {
		Recomendacao recomendacao = new Recomendacao(produto);
		recomendacoes.add(recomendacao);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produto other = (Produto) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("codigo", codigo)
				.add("nome", nome).add("foto", foto).toString();
	}
}

package br.edu.ufabc.crawler;

import br.edu.ufabc.crawler.robot.Robot;
import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;

public class Crawler {

	private static final int NUMBER_OF_CRAWLERS = 1;

	private static final String STORAGE_FOLDER = "crawler-data";

	private static final int MAXIMUM_PAGES = 10;

	private static final String URL_INICIAL = "http://www.amazon.com.br/Vigiar-Punir-Hist%C3%B3ria-Viol%C3%AAncia-Pris%C3%B5es/dp/8532605087/ref=sr_1_1?s=books&ie=UTF8&qid=1438463698&sr=1-1&keywords=vigiar+e+punir";

	public static void main(String[] args) throws Exception {
		CrawlConfig config = new CrawlConfig();
		config.setCrawlStorageFolder(STORAGE_FOLDER);
		config.setMaxPagesToFetch(MAXIMUM_PAGES);

		PageFetcher pageFetcher = new PageFetcher(config);
		RobotstxtConfig robotsTxtConfig = new RobotstxtConfig();
		RobotstxtServer robotsTxtServer = new RobotstxtServer(robotsTxtConfig,
				pageFetcher);
		CrawlController controller = new CrawlController(config, pageFetcher,
				robotsTxtServer);

		controller.addSeed(URL_INICIAL);

		controller.start(Robot.class, NUMBER_OF_CRAWLERS);
	}

}

package br.edu.ufabc.crawler.pagina;

import java.util.HashSet;
import java.util.Set;

import br.edu.ufabc.crawler.url.URL;
import edu.uci.ics.crawler4j.url.WebURL;

public class PaginaDeProduto {

	private URL endereco;

	private String codigoFonte;

	private Set<Pagina> produtosRelacionados;

	public PaginaDeProduto(String endereco, byte[] codigoFonte,
			Set<WebURL> urlsDeSaida) {
		this.endereco = new URL(endereco);
		this.codigoFonte = new String(codigoFonte);

		this.produtosRelacionados = new HashSet<Pagina>();

		for (WebURL url : urlsDeSaida) {
			Pagina pagina = new Pagina(url.getURL());
			if (pagina.isProduto() && !pagina.equals(this)) {
				produtosRelacionados.add(pagina);
			}
		}

	}

	public URL getEndereco() {
		return endereco;
	}

	public String getCodigoFonte() {
		return codigoFonte;
	}

	public Set<Pagina> getProdutosRecomendados() {
		return produtosRelacionados;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((endereco == null) ? 0 : endereco.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaginaDeProduto other = (PaginaDeProduto) obj;
		if (endereco == null) {
			if (other.endereco != null)
				return false;
		} else if (!endereco.equals(other.endereco))
			return false;
		return true;
	}

}

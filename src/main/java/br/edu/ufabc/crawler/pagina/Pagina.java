package br.edu.ufabc.crawler.pagina;

import br.edu.ufabc.crawler.url.URL;

public class Pagina {

	private URL endereco;

	public Pagina(String endereco) {
		this.endereco = new URL(endereco);
	}

	public boolean isProduto() {
		return endereco.getValor().contains("/dp/");
	}

	public URL getEndereco() {
		return endereco;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((endereco == null) ? 0 : endereco.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pagina other = (Pagina) obj;
		if (endereco == null) {
			if (other.endereco != null)
				return false;
		} else if (!endereco.equals(other.endereco))
			return false;
		return true;
	}

}
package br.edu.ufabc.crawler.robot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.edu.ufabc.crawler.graph.ProdutosDao;
import br.edu.ufabc.crawler.pagina.Pagina;
import br.edu.ufabc.crawler.pagina.PaginaDeProduto;
import br.edu.ufabc.crawler.produto.Produto;
import br.edu.ufabc.crawler.produto.Produtos;
import br.edu.ufabc.crawler.scraper.ProdutoScraper;
import br.edu.ufabc.crawler.scraper.RecomendacoesScraper;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.url.WebURL;

public class Robot extends WebCrawler {

	private static final Logger logger = LoggerFactory.getLogger(Robot.class);

	@Override
	public boolean shouldVisit(Page page, WebURL url) {
		Pagina pagina = new Pagina(url.getURL());
		return pagina.isProduto();
	}

	@Override
	public void visit(Page page) {
		PaginaDeProduto pagina = new PaginaDeProduto(
				page.getWebURL().getPath(), page.getContentData(), page
						.getParseData().getOutgoingUrls());

		logger.info("Crawleando a página: " + pagina.getEndereco());

		ProdutoScraper produtoScraper = new ProdutoScraper();
		Produto produto = produtoScraper.scrap(pagina);

		RecomendacoesScraper recomendacoesScraper = new RecomendacoesScraper();
		recomendacoesScraper.scrap(produto, pagina);

		Produtos produtosDao = new ProdutosDao();
		produtosDao.salva(produto);
	}
}

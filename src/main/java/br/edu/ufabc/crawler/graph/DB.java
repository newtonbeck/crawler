package br.edu.ufabc.crawler.graph;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

public class DB {

	private static final String CAMINHO = "/home/newton/my-db";

	private static final GraphDatabaseFactory FACTORY = new GraphDatabaseFactory();

	private static GraphDatabaseService db;

	public static synchronized GraphDatabaseService get() {
		if (db == null) {
			db = FACTORY.newEmbeddedDatabase(CAMINHO);
		}
		return db;
	}

}

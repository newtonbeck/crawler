package br.edu.ufabc.crawler.graph;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;

import br.edu.ufabc.crawler.produto.Produto;
import br.edu.ufabc.crawler.produto.Produtos;
import br.edu.ufabc.crawler.produto.Recomendacao;

public class ProdutosDao implements Produtos {

	private GraphDatabaseService db;

	public ProdutosDao() {
		db = DB.get();
	}

	@Override
	public void salva(Produto produto) {
		try (Transaction tx = db.beginTx()) {
			Node no = db.findNode(Labels.Produto, "codigo", produto.getCodigo()
					.getValor());
			if (no == null) {
				no = db.createNode(Labels.Produto);
			}

			no.setProperty("codigo", produto.getCodigo().getValor());
			no.setProperty("nome", produto.getNome());
			no.setProperty("foto", produto.getFoto().getEndereco().getValor());

			for (Recomendacao recomendacao : produto.getRecomendacoes()) {
				Produto recomendado = recomendacao.getProduto();
				Node noRecomendado = db.findNode(Labels.Produto, "codigo",
						recomendado.getCodigo().getValor());
				if (noRecomendado == null) {
					noRecomendado = db.createNode(Labels.Produto);
					noRecomendado.setProperty("codigo", recomendado.getCodigo()
							.getValor());
				}

				no.createRelationshipTo(noRecomendado, Relacoes.RECOMENDA);
			}

			tx.success();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}

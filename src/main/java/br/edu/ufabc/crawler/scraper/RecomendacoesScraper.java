package br.edu.ufabc.crawler.scraper;

import br.edu.ufabc.crawler.pagina.Pagina;
import br.edu.ufabc.crawler.pagina.PaginaDeProduto;
import br.edu.ufabc.crawler.produto.Codigo;
import br.edu.ufabc.crawler.produto.Produto;

public class RecomendacoesScraper {

	public void scrap(Produto produto, PaginaDeProduto pagina) {

		for (Pagina paginaDeProdutoRecomendado : pagina
				.getProdutosRecomendados()) {
			Codigo codigo = Codigo.da(paginaDeProdutoRecomendado
					.getEndereco());
			Produto produtoRecomendado = new Produto(codigo);
			produto.recomenda(produtoRecomendado);
		}

	}

}

package br.edu.ufabc.crawler.scraper;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import br.edu.ufabc.crawler.pagina.PaginaDeProduto;
import br.edu.ufabc.crawler.produto.Codigo;
import br.edu.ufabc.crawler.produto.Foto;
import br.edu.ufabc.crawler.produto.Produto;
import br.edu.ufabc.crawler.url.URL;

public class ProdutoScraper {

	public Produto scrap(PaginaDeProduto pagina) {
		Document documento = Jsoup.parse(pagina.getCodigoFonte());

		Codigo codigo = Codigo.da(pagina.getEndereco());
		Produto produto = new Produto(codigo);

		String nome = documento.getElementById("btAsinTitle").text();
		produto.setNome(nome);

		URL urlDaFoto = new URL(documento.getElementById("main-image").attr(
				"src"));
		Foto foto = new Foto(urlDaFoto);
		produto.setFoto(foto);

		return produto;
	}

}
